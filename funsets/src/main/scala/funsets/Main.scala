package funsets

object Main extends App {

	import FunSets._
	
	println(contains(singletonSet(1), 1))
	println(contains(singletonSet(2), 1))

	val s1 = singletonSet(1)
	val s2 = singletonSet(2)
	val s3 = singletonSet(3)
	val empty = emptySet()

	print("SingletonSet of 1: "); printSet(s1)

	// Unions
	val unionS1S2 = union(s1, s2)
	val unionS1S3 = union(s1, s3)
	val unionS1S2S3 = union(unionS1S2, s3)

	print("union of S1 and S2: "); printSet(unionS1S2)
	print("union of S1, S2 and S3: "); printSet(unionS1S2S3)

	// Intersects
	val intersectS1S2 = intersect(s1, s2)
	val intersectS1UnionS1S2S3 = intersect(s1, unionS1S2S3)

	print("intersect of S1 and S2: "); printSet(intersectS1S2)
	print("intersect of S1 and UnionS1S2S3: "); printSet(intersectS1UnionS1S2S3)

	// diff
	val diffS1S2 = diff(s1, s2)
	val diffS1S3 = diff(s1, s3)
	val diffUnionS1S2S3S1 = diff(unionS1S2S3, s1)

	print("diff of S1 and S2: "); printSet(diffS1S2)
	print("diff of UnionS1S2S3 with S1: "); printSet(diffUnionS1S2S3S1)

  	print("filter Union of S1, S2 and S by x!=1: "); printSet(filter(unionS1S2S3, (x => x!=1)))

  	println("Union of s1, s2 and s3 has at least one element that is greater than 1: " + exists(unionS1S2S3, (x => x > 1)))

  	print("map Union of S1, S2 and S to x+1: "); printSet(map(unionS1S2S3, x => x + 1))

	val s = union(union(union(union(union(singletonSet(1), singletonSet(3)), singletonSet(4)), singletonSet(5)), singletonSet(7)), singletonSet(1000))
	printSet(s)
  	printSet(map(s, x => x-1))
    
}
