package funsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {


  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.8/index.html#org.scalatest.FunSuite
   *
   * Operators
   *  - test
   *  - ignore
   *  - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
  test("string take") {
    val message = "hello, world"
    assert(message.take(5) == "hello")
  }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
  test("adding ints") {
    assert(1 + 2 === 3)
  }

  
  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }
  
  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   * 
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   * 
   *   val s1 = singletonSet(1)
   * 
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   * 
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   * 
   */

  trait TestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val empty = emptySet()
    
    // Unions
    val unionS1S2 = union(s1, s2)
    val unionS1S3 = union(s1, s3)
    val unionS1S2S3 = union(unionS1S2, s3)
    
    // Intersects
    val intersectS1S2 = intersect(s1, s2)
    val intersectS1UnionS1S2S3 = intersect(s1, unionS1S2S3)

    // diff
    val diffS1S2 = diff(s1, s2)
    val diffS1S3 = diff(s1, s3)
    val diffUnionS1S2S3S1 = diff(unionS1S2S3, s1)

    // filter
    val filter1 = filter(unionS1S2S3, (x => x == 1))
    val filter2 = filter(unionS1S2S3, (x => x != 1))

  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   * 
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet tests") {
    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3". 
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "SingletonSet 1 contains 1.")
      assert(!contains(s1, 2), "SingletonSet 1 does not contain 2.")
      assert(!contains(empty, 1), "empty set should not contain anything.")
    }
  }

  test("union contains all elements") {
    new TestSets {
      assert(contains(unionS1S2, 1), "Union of s1 and s2 should contain 1")
      assert(contains(unionS1S2, 2), "Union of s1 and s2 should contain 2")
      assert(!contains(unionS1S2, 3), "Union of s1 and s2 should not contain 3")
      assert(contains(unionS1S2S3, 3), "Union of s1, s2 and s3 should contain 3.")
      assert(contains(union(empty, s1), 1), "Union of s1 and empty set should contain 1.")
    }
  }

  test("intersect contains the intersection of two sets") {
    new TestSets {
      assert(!contains(intersectS1S2, 1), "Intersection of s1 and s2 should not contain 1.")
      assert(contains(intersectS1UnionS1S2S3, 1), "Intersection of s1 and union of s1, s2 and s3 should contain 1.")
      assert(!contains(intersect(empty, s1), 1), "Intersection of s1 and empty set should be empty set and not contain 1.")
    }
  }

  test("diff contains the difference of the two given sets") {
    new TestSets {
      assert(contains(diffS1S2, 1), "difference of s1 and s2 should contain 1.")
      assert(!contains(diffUnionS1S2S3S1, 1), "difference of union of s1, s2 and s3 with s1 should not contain 1.")
    }
  }

  test("filter contains the subset of the set for which a predicate holds.") {
    new TestSets {
      assert(contains(filter1, 1), "filtering the set of s1, s2 and s3 by x==1 should contain 1.")
      assert(!contains(filter1, 2), "filtering the set of s1, s2 and s3 by x==1 should not contain 2.")
      assert(!contains(filter2, 1), "filtering the set of s1, s2 and s3 by x!=1 should not contain 1.")
      assert(contains(filter2, 2), "filtering the set of s1, s2 and s3 by x!=1 should contain 2.")
      assert(contains(filter2, 3), "filtering the set of s1, s2 and s3 by x!=1 should contain 3.")
    }
  }

  test("forall returns whether all bounded integers within `s` satisfy `p`") {
    new TestSets {
      assert(forall(unionS1S2S3, (x => x < 5)), "all members of Union of s1, s2 and s3 should be less than 5.")
      assert(!forall(unionS1S2S3, (x => x < 3)), "all members of Union of s1, s2 and s3 are not less than 3.")
      assert(forall(empty, (x => true)), "nothing is true for empty set.")
    }
  }

  test("exists returns whether there exists a bounded integer within `s` that satisfies `p`.") {
    new TestSets {
      assert(exists(unionS1S2S3, (x => x > 1)), "Union of s1, s2 and s3 has at least one element that is greater than 1.")
      assert(!exists(unionS1S2S3, (x => x < 1)), "Union of s1, s2 and s3 does not have any element that is less than 1.")
      assert(!exists(empty, (x => true)), "empty set contains nothing.")
    }
  }

  test("map returns a set transformed by applying `f` to each element of `s`.") {
    new TestSets {
      assert(contains(map(unionS1S2S3, x => x + 1), 4), "mapping Union of S1, S2 and S to x+1 contains 4.")
      assert(!contains(map(unionS1S2S3, x => x + 1), 1), "mapping Union of S1, S2 and S to x+1 does not contain 1.")
      assert(!contains(map(empty, x => x + 1), 1), "mapping empty set to x+1 does not contain 1.")
    }
  }

}
