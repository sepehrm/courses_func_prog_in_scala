import patmat.Huffman._

object work {import scala.runtime.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(73); 

  val list1 = string2Chars("abc");System.out.println("""list1  : List[Char] = """ + $show(list1 ));$skip(36); 
  val list2 = string2Chars("hello");System.out.println("""list2  : List[Char] = """ + $show(list2 ));$skip(49); 
  val list3 = string2Chars("this is a sentence");System.out.println("""list3  : List[Char] = """ + $show(list3 ));$skip(40); 
  val list4 = string2Chars("telephone");System.out.println("""list4  : List[Char] = """ + $show(list4 ));$skip(41); 
  val list5 = string2Chars("california");System.out.println("""list5  : List[Char] = """ + $show(list5 ));$skip(56); 
  val list6 = string2Chars("mississippi state college");System.out.println("""list6  : List[Char] = """ + $show(list6 ));$skip(13); val res$0 = 
  list1.size;System.out.println("""res0: Int = """ + $show(res$0));$skip(13); val res$1 = 
  list2.size;System.out.println("""res1: Int = """ + $show(res$1));$skip(13); val res$2 = 
  list3.size;System.out.println("""res2: Int = """ + $show(res$2));$skip(13); val res$3 = 
  list4.size;System.out.println("""res3: Int = """ + $show(res$3));$skip(13); val res$4 = 
  list5.size;System.out.println("""res4: Int = """ + $show(res$4));$skip(13); val res$5 = 
  list6.size;System.out.println("""res5: Int = """ + $show(res$5));$skip(28); 
  val times1 = times(list1);System.out.println("""times1  : List[(Char, Int)] = """ + $show(times1 ));$skip(28); 
  val times2 = times(list2);System.out.println("""times2  : List[(Char, Int)] = """ + $show(times2 ));$skip(28); 
  val times3 = times(list3);System.out.println("""times3  : List[(Char, Int)] = """ + $show(times3 ));$skip(28); 
  val times4 = times(list4);System.out.println("""times4  : List[(Char, Int)] = """ + $show(times4 ));$skip(28); 
  val times5 = times(list5);System.out.println("""times5  : List[(Char, Int)] = """ + $show(times5 ));$skip(28); 
  val times6 = times(list6);System.out.println("""times6  : List[(Char, Int)] = """ + $show(times6 ));$skip(41); 
  val oll1 = makeOrderedLeafList(times1);System.out.println("""oll1  : List[patmat.Huffman.Leaf] = """ + $show(oll1 ));$skip(41); 
  val oll2 = makeOrderedLeafList(times2);System.out.println("""oll2  : List[patmat.Huffman.Leaf] = """ + $show(oll2 ));$skip(41); 
  val oll3 = makeOrderedLeafList(times3);System.out.println("""oll3  : List[patmat.Huffman.Leaf] = """ + $show(oll3 ));$skip(41); 
  val oll4 = makeOrderedLeafList(times4);System.out.println("""oll4  : List[patmat.Huffman.Leaf] = """ + $show(oll4 ));$skip(41); 
  val oll5 = makeOrderedLeafList(times5);System.out.println("""oll5  : List[patmat.Huffman.Leaf] = """ + $show(oll5 ));$skip(41); 
  val oll6 = makeOrderedLeafList(times6);System.out.println("""oll6  : List[patmat.Huffman.Leaf] = """ + $show(oll6 ));$skip(17); val res$6 = 

  combine(oll1);System.out.println("""res6: List[patmat.Huffman.CodeTree] = """ + $show(res$6));$skip(16); val res$7 = 
  combine(oll2);System.out.println("""res7: List[patmat.Huffman.CodeTree] = """ + $show(res$7));$skip(16); val res$8 = 
  combine(oll3);System.out.println("""res8: List[patmat.Huffman.CodeTree] = """ + $show(res$8));$skip(16); val res$9 = 
  combine(oll4);System.out.println("""res9: List[patmat.Huffman.CodeTree] = """ + $show(res$9));$skip(16); val res$10 = 
  combine(oll5);System.out.println("""res10: List[patmat.Huffman.CodeTree] = """ + $show(res$10));$skip(16); val res$11 = 
  combine(oll6);System.out.println("""res11: List[patmat.Huffman.CodeTree] = """ + $show(res$11));$skip(35); 

  val ct1 = createCodeTree(list1);System.out.println("""ct1  : patmat.Huffman.CodeTree = """ + $show(ct1 ));$skip(34); 
  val ct2 = createCodeTree(list2);System.out.println("""ct2  : patmat.Huffman.CodeTree = """ + $show(ct2 ));$skip(34); 
  val ct3 = createCodeTree(list3);System.out.println("""ct3  : patmat.Huffman.CodeTree = """ + $show(ct3 ));$skip(34); 
  val ct4 = createCodeTree(list4);System.out.println("""ct4  : patmat.Huffman.CodeTree = """ + $show(ct4 ));$skip(34); 
  val ct5 = createCodeTree(list5);System.out.println("""ct5  : patmat.Huffman.CodeTree = """ + $show(ct5 ));$skip(34); 
  val ct6 = createCodeTree(list6);System.out.println("""ct6  : patmat.Huffman.CodeTree = """ + $show(ct6 ));$skip(45); 


  val e11 = encode(ct1)(string2Chars("a"));System.out.println("""e11  : List[patmat.Huffman.Bit] = """ + $show(e11 ));$skip(43); 
  val e12 = encode(ct1)(string2Chars("b"));System.out.println("""e12  : List[patmat.Huffman.Bit] = """ + $show(e12 ));$skip(43); 
  val e13 = encode(ct1)(string2Chars("c"));System.out.println("""e13  : List[patmat.Huffman.Bit] = """ + $show(e13 ));$skip(44); 
  val e14 = encode(ct1)(string2Chars("ab"));System.out.println("""e14  : List[patmat.Huffman.Bit] = """ + $show(e14 ));$skip(44); 
  val e15 = encode(ct1)(string2Chars("bc"));System.out.println("""e15  : List[patmat.Huffman.Bit] = """ + $show(e15 ));$skip(45); 
  val e16 = encode(ct1)(string2Chars("abc"));System.out.println("""e16  : List[patmat.Huffman.Bit] = """ + $show(e16 ));$skip(48); 
  val e17 = encode(ct1)(string2Chars("abcabc"));System.out.println("""e17  : List[patmat.Huffman.Bit] = """ + $show(e17 ));$skip(20); val res$12 = 

  decode(ct1, e11);System.out.println("""res12: List[Char] = """ + $show(res$12));$skip(19); val res$13 = 
  decode(ct1, e12);System.out.println("""res13: List[Char] = """ + $show(res$13));$skip(19); val res$14 = 
  decode(ct1, e13);System.out.println("""res14: List[Char] = """ + $show(res$14));$skip(19); val res$15 = 
  decode(ct1, e14);System.out.println("""res15: List[Char] = """ + $show(res$15));$skip(19); val res$16 = 
  decode(ct1, e15);System.out.println("""res16: List[Char] = """ + $show(res$16));$skip(19); val res$17 = 
  decode(ct1, e16);System.out.println("""res17: List[Char] = """ + $show(res$17));$skip(19); val res$18 = 
  decode(ct1, e17);System.out.println("""res18: List[Char] = """ + $show(res$18));$skip(44); 

  val e21 = encode(ct2)(string2Chars("h"));System.out.println("""e21  : List[patmat.Huffman.Bit] = """ + $show(e21 ));$skip(43); 
  val e22 = encode(ct2)(string2Chars("e"));System.out.println("""e22  : List[patmat.Huffman.Bit] = """ + $show(e22 ));$skip(43); 
  val e23 = encode(ct2)(string2Chars("l"));System.out.println("""e23  : List[patmat.Huffman.Bit] = """ + $show(e23 ));$skip(43); 
  val e24 = encode(ct2)(string2Chars("o"));System.out.println("""e24  : List[patmat.Huffman.Bit] = """ + $show(e24 ));$skip(44); 
  val e25 = encode(ct2)(string2Chars("he"));System.out.println("""e25  : List[patmat.Huffman.Bit] = """ + $show(e25 ));$skip(47); 
  val e26 = encode(ct2)(string2Chars("hello"));System.out.println("""e26  : List[patmat.Huffman.Bit] = """ + $show(e26 ));$skip(52); 
  val e27 = encode(ct2)(string2Chars("hellohello"));System.out.println("""e27  : List[patmat.Huffman.Bit] = """ + $show(e27 ));$skip(20); val res$19 = 

  decode(ct2, e21);System.out.println("""res19: List[Char] = """ + $show(res$19));$skip(19); val res$20 = 
  decode(ct2, e22);System.out.println("""res20: List[Char] = """ + $show(res$20));$skip(19); val res$21 = 
  decode(ct2, e23);System.out.println("""res21: List[Char] = """ + $show(res$21));$skip(19); val res$22 = 
  decode(ct2, e24);System.out.println("""res22: List[Char] = """ + $show(res$22));$skip(19); val res$23 = 
  decode(ct2, e25);System.out.println("""res23: List[Char] = """ + $show(res$23));$skip(19); val res$24 = 
  decode(ct2, e26);System.out.println("""res24: List[Char] = """ + $show(res$24));$skip(19); val res$25 = 
  decode(ct2, e27);System.out.println("""res25: List[Char] = """ + $show(res$25));$skip(19); val res$26 = 


    convert(ct1);System.out.println("""res26: patmat.Huffman.CodeTable = """ + $show(res$26));$skip(17); val res$27 = 
    convert(ct2);System.out.println("""res27: patmat.Huffman.CodeTable = """ + $show(res$27));$skip(17); val res$28 = 
    convert(ct3);System.out.println("""res28: patmat.Huffman.CodeTable = """ + $show(res$28));$skip(17); val res$29 = 
    convert(ct4);System.out.println("""res29: patmat.Huffman.CodeTable = """ + $show(res$29));$skip(17); val res$30 = 
    convert(ct5);System.out.println("""res30: patmat.Huffman.CodeTable = """ + $show(res$30));$skip(17); val res$31 = 
    convert(ct6);System.out.println("""res31: patmat.Huffman.CodeTable = """ + $show(res$31));$skip(49); 

  val e61 = quickEncode(ct6)(string2Chars("m"));System.out.println("""e61  : List[patmat.Huffman.Bit] = """ + $show(e61 ));$skip(48); 
  val e62 = quickEncode(ct6)(string2Chars("i"));System.out.println("""e62  : List[patmat.Huffman.Bit] = """ + $show(e62 ));$skip(48); 
  val e63 = quickEncode(ct6)(string2Chars("s"));System.out.println("""e63  : List[patmat.Huffman.Bit] = """ + $show(e63 ));$skip(51); 
  val e64 = quickEncode(ct6)(string2Chars("miss"));System.out.println("""e64  : List[patmat.Huffman.Bit] = """ + $show(e64 ));$skip(53); 
  val e65 = quickEncode(ct6)(string2Chars("llissm"));System.out.println("""e65  : List[patmat.Huffman.Bit] = """ + $show(e65 ));$skip(56); 
  val e66 = quickEncode(ct6)(string2Chars("misisisis"));System.out.println("""e66  : List[patmat.Huffman.Bit] = """ + $show(e66 ));$skip(53); 
  val e67 = quickEncode(ct6)(string2Chars("mmmmmm"));System.out.println("""e67  : List[patmat.Huffman.Bit] = """ + $show(e67 ));$skip(72); 
  val e68 = quickEncode(ct6)(string2Chars("mississippi state college"));System.out.println("""e68  : List[patmat.Huffman.Bit] = """ + $show(e68 ));$skip(20); val res$32 = 

  decode(ct6, e61);System.out.println("""res32: List[Char] = """ + $show(res$32));$skip(19); val res$33 = 
  decode(ct6, e62);System.out.println("""res33: List[Char] = """ + $show(res$33));$skip(19); val res$34 = 
  decode(ct6, e63);System.out.println("""res34: List[Char] = """ + $show(res$34));$skip(19); val res$35 = 
  decode(ct6, e64);System.out.println("""res35: List[Char] = """ + $show(res$35));$skip(19); val res$36 = 
  decode(ct6, e65);System.out.println("""res36: List[Char] = """ + $show(res$36));$skip(19); val res$37 = 
  decode(ct6, e66);System.out.println("""res37: List[Char] = """ + $show(res$37));$skip(19); val res$38 = 
  decode(ct6, e67);System.out.println("""res38: List[Char] = """ + $show(res$38));$skip(19); val res$39 = 
  decode(ct6, e68);System.out.println("""res39: List[Char] = """ + $show(res$39))}

}