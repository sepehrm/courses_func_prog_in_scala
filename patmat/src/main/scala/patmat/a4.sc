import patmat.Huffman._

object work {

  val list1 = string2Chars("abc")                 //> list1  : List[Char] = List(a, b, c)
  val list2 = string2Chars("hello")               //> list2  : List[Char] = List(h, e, l, l, o)
  val list3 = string2Chars("this is a sentence")  //> list3  : List[Char] = List(t, h, i, s,  , i, s,  , a,  , s, e, n, t, e, n, c
                                                  //| , e)
  val list4 = string2Chars("telephone")           //> list4  : List[Char] = List(t, e, l, e, p, h, o, n, e)
  val list5 = string2Chars("california")          //> list5  : List[Char] = List(c, a, l, i, f, o, r, n, i, a)
  val list6 = string2Chars("mississippi state college")
                                                  //> list6  : List[Char] = List(m, i, s, s, i, s, s, i, p, p, i,  , s, t, a, t, e
                                                  //| ,  , c, o, l, l, e, g, e)
  list1.size                                      //> res0: Int = 3
  list2.size                                      //> res1: Int = 5
  list3.size                                      //> res2: Int = 18
  list4.size                                      //> res3: Int = 9
  list5.size                                      //> res4: Int = 10
  list6.size                                      //> res5: Int = 25
  val times1 = times(list1)                       //> times1  : List[(Char, Int)] = List((a,1), (b,1), (c,1))
  val times2 = times(list2)                       //> times2  : List[(Char, Int)] = List((h,1), (e,1), (l,2), (o,1))
  val times3 = times(list3)                       //> times3  : List[(Char, Int)] = List((t,2), (h,1), (i,2), (s,3), ( ,3), (a,1),
                                                  //|  (e,3), (n,2), (c,1))
  val times4 = times(list4)                       //> times4  : List[(Char, Int)] = List((t,1), (e,3), (l,1), (p,1), (h,1), (o,1),
                                                  //|  (n,1))
  val times5 = times(list5)                       //> times5  : List[(Char, Int)] = List((c,1), (a,2), (l,1), (i,2), (f,1), (o,1),
                                                  //|  (r,1), (n,1))
  val times6 = times(list6)                       //> times6  : List[(Char, Int)] = List((m,1), (i,4), (s,5), (p,2), ( ,2), (t,2),
                                                  //|  (a,1), (e,3), (c,1), (o,1), (l,2), (g,1))
  val oll1 = makeOrderedLeafList(times1)          //> oll1  : List[patmat.Huffman.Leaf] = List(Leaf(a,1), Leaf(b,1), Leaf(c,1))
  val oll2 = makeOrderedLeafList(times2)          //> oll2  : List[patmat.Huffman.Leaf] = List(Leaf(h,1), Leaf(e,1), Leaf(o,1), Le
                                                  //| af(l,2))
  val oll3 = makeOrderedLeafList(times3)          //> oll3  : List[patmat.Huffman.Leaf] = List(Leaf(h,1), Leaf(a,1), Leaf(c,1), Le
                                                  //| af(t,2), Leaf(i,2), Leaf(n,2), Leaf(s,3), Leaf( ,3), Leaf(e,3))
  val oll4 = makeOrderedLeafList(times4)          //> oll4  : List[patmat.Huffman.Leaf] = List(Leaf(t,1), Leaf(l,1), Leaf(p,1), Le
                                                  //| af(h,1), Leaf(o,1), Leaf(n,1), Leaf(e,3))
  val oll5 = makeOrderedLeafList(times5)          //> oll5  : List[patmat.Huffman.Leaf] = List(Leaf(c,1), Leaf(l,1), Leaf(f,1), Le
                                                  //| af(o,1), Leaf(r,1), Leaf(n,1), Leaf(a,2), Leaf(i,2))
  val oll6 = makeOrderedLeafList(times6)          //> oll6  : List[patmat.Huffman.Leaf] = List(Leaf(m,1), Leaf(a,1), Leaf(c,1), Le
                                                  //| af(o,1), Leaf(g,1), Leaf(p,2), Leaf( ,2), Leaf(t,2), Leaf(l,2), Leaf(e,3), L
                                                  //| eaf(i,4), Leaf(s,5))

  combine(oll1)                                   //> res6: List[patmat.Huffman.CodeTree] = List(Fork(Leaf(a,1),Leaf(b,1),List(a, 
                                                  //| b),2), Leaf(c,1))
  combine(oll2)                                   //> res7: List[patmat.Huffman.CodeTree] = List(Fork(Leaf(h,1),Leaf(e,1),List(h, 
                                                  //| e),2), Leaf(o,1), Leaf(l,2))
  combine(oll3)                                   //> res8: List[patmat.Huffman.CodeTree] = List(Fork(Leaf(h,1),Leaf(a,1),List(h, 
                                                  //| a),2), Leaf(c,1), Leaf(t,2), Leaf(i,2), Leaf(n,2), Leaf(s,3), Leaf( ,3), Lea
                                                  //| f(e,3))
  combine(oll4)                                   //> res9: List[patmat.Huffman.CodeTree] = List(Fork(Leaf(t,1),Leaf(l,1),List(t, 
                                                  //| l),2), Leaf(p,1), Leaf(h,1), Leaf(o,1), Leaf(n,1), Leaf(e,3))
  combine(oll5)                                   //> res10: List[patmat.Huffman.CodeTree] = List(Fork(Leaf(c,1),Leaf(l,1),List(c,
                                                  //|  l),2), Leaf(f,1), Leaf(o,1), Leaf(r,1), Leaf(n,1), Leaf(a,2), Leaf(i,2))
  combine(oll6)                                   //> res11: List[patmat.Huffman.CodeTree] = List(Fork(Leaf(m,1),Leaf(a,1),List(m,
                                                  //|  a),2), Leaf(c,1), Leaf(o,1), Leaf(g,1), Leaf(p,2), Leaf( ,2), Leaf(t,2), Le
                                                  //| af(l,2), Leaf(e,3), Leaf(i,4), Leaf(s,5))

  val ct1 = createCodeTree(list1)                 //> ct1  : patmat.Huffman.CodeTree = Fork(Fork(Leaf(a,1),Leaf(b,1),List(a, b),2)
                                                  //| ,Leaf(c,1),List(a, b, c),3)
  val ct2 = createCodeTree(list2)                 //> ct2  : patmat.Huffman.CodeTree = Fork(Fork(Fork(Leaf(h,1),Leaf(e,1),List(h, 
                                                  //| e),2),Leaf(o,1),List(h, e, o),3),Leaf(l,2),List(h, e, o, l),5)
  val ct3 = createCodeTree(list3)                 //> ct3  : patmat.Huffman.CodeTree = Fork(Fork(Fork(Fork(Fork(Fork(Fork(Fork(Lea
                                                  //| f(h,1),Leaf(a,1),List(h, a),2),Leaf(c,1),List(h, a, c),3),Leaf(t,2),List(h, 
                                                  //| a, c, t),5),Leaf(i,2),List(h, a, c, t, i),7),Leaf(n,2),List(h, a, c, t, i, n
                                                  //| ),9),Leaf(s,3),List(h, a, c, t, i, n, s),12),Leaf( ,3),List(h, a, c, t, i, n
                                                  //| , s,  ),15),Leaf(e,3),List(h, a, c, t, i, n, s,  , e),18)
  val ct4 = createCodeTree(list4)                 //> ct4  : patmat.Huffman.CodeTree = Fork(Fork(Fork(Fork(Fork(Fork(Leaf(t,1),Le
                                                  //| af(l,1),List(t, l),2),Leaf(p,1),List(t, l, p),3),Leaf(h,1),List(t, l, p, h)
                                                  //| ,4),Leaf(o,1),List(t, l, p, h, o),5),Leaf(n,1),List(t, l, p, h, o, n),6),Le
                                                  //| af(e,3),List(t, l, p, h, o, n, e),9)
  val ct5 = createCodeTree(list5)                 //> ct5  : patmat.Huffman.CodeTree = Fork(Fork(Fork(Fork(Fork(Fork(Fork(Leaf(c,
                                                  //| 1),Leaf(l,1),List(c, l),2),Leaf(f,1),List(c, l, f),3),Leaf(o,1),List(c, l, 
                                                  //| f, o),4),Leaf(r,1),List(c, l, f, o, r),5),Leaf(n,1),List(c, l, f, o, r, n),
                                                  //| 6),Leaf(a,2),List(c, l, f, o, r, n, a),8),Leaf(i,2),List(c, l, f, o, r, n, 
                                                  //| a, i),10)
  val ct6 = createCodeTree(list6)                 //> ct6  : patmat.Huffman.CodeTree = Fork(Fork(Fork(Fork(Fork(Fork(Fork(Fork(Fo
                                                  //| rk(Fork(Fork(Leaf(m,1),Leaf(a,1),List(m, a),2),Leaf(c,1),List(m, a, c),3),L
                                                  //| eaf(o,1),List(m, a, c, o),4),Leaf(g,1),List(m, a, c, o, g),5),Leaf(p,2),Lis
                                                  //| t(m, a, c, o, g, p),7),Leaf( ,2),List(m, a, c, o, g, p,  ),9),Leaf(t,2),Lis
                                                  //| t(m, a, c, o, g, p,  , t),11),Leaf(l,2),List(m, a, c, o, g, p,  , t, l),13)
                                                  //| ,Leaf(e,3),List(m, a, c, o, g, p,  , t, l, e),16),Leaf(i,4),List(m, a, c, o
                                                  //| , g, p,  , t, l, e, i),20),Leaf(s,5),List(m, a, c, o, g, p,  , t, l, e, i, 
                                                  //| s),25)


  val e11 = encode(ct1)(string2Chars("a"))        //> java.lang.Error: an implementation is missing
                                                  //| 	at common.package$.$qmark$qmark$qmark(package.scala:5)
                                                  //| 	at patmat.Huffman$.encode(Huffman.scala:185)
                                                  //| 	at work$$anonfun$main$1.apply$mcV$sp(work.scala:45)
                                                  //| 	at scala.runtime.WorksheetSupport$$anonfun$$execute$1.apply$mcV$sp(Works
                                                  //| heetSupport.scala:75)
                                                  //| 	at scala.runtime.WorksheetSupport$.redirected(WorksheetSupport.scala:64)
                                                  //| 
                                                  //| 	at scala.runtime.WorksheetSupport$.$execute(WorksheetSupport.scala:74)
                                                  //| 	at work$.main(work.scala:3)
                                                  //| 	at work.main(work.scala)
  val e12 = encode(ct1)(string2Chars("b"))
  val e13 = encode(ct1)(string2Chars("c"))
  val e14 = encode(ct1)(string2Chars("ab"))
  val e15 = encode(ct1)(string2Chars("bc"))
  val e16 = encode(ct1)(string2Chars("abc"))
  val e17 = encode(ct1)(string2Chars("abcabc"))

  decode(ct1, e11)
  decode(ct1, e12)
  decode(ct1, e13)
  decode(ct1, e14)
  decode(ct1, e15)
  decode(ct1, e16)
  decode(ct1, e17)

  val e21 = encode(ct2)(string2Chars("h"))
  val e22 = encode(ct2)(string2Chars("e"))
  val e23 = encode(ct2)(string2Chars("l"))
  val e24 = encode(ct2)(string2Chars("o"))
  val e25 = encode(ct2)(string2Chars("he"))
  val e26 = encode(ct2)(string2Chars("hello"))
  val e27 = encode(ct2)(string2Chars("hellohello"))

  decode(ct2, e21)
  decode(ct2, e22)
  decode(ct2, e23)
  decode(ct2, e24)
  decode(ct2, e25)
  decode(ct2, e26)
  decode(ct2, e27)


    convert(ct1)
    convert(ct2)
    convert(ct3)
    convert(ct4)
    convert(ct5)
    convert(ct6)

  val e61 = quickEncode(ct6)(string2Chars("m"))
  val e62 = quickEncode(ct6)(string2Chars("i"))
  val e63 = quickEncode(ct6)(string2Chars("s"))
  val e64 = quickEncode(ct6)(string2Chars("miss"))
  val e65 = quickEncode(ct6)(string2Chars("llissm"))
  val e66 = quickEncode(ct6)(string2Chars("misisisis"))
  val e67 = quickEncode(ct6)(string2Chars("mmmmmm"))
  val e68 = quickEncode(ct6)(string2Chars("mississippi state college"))

  decode(ct6, e61)
  decode(ct6, e62)
  decode(ct6, e63)
  decode(ct6, e64)
  decode(ct6, e65)
  decode(ct6, e66)
  decode(ct6, e67)
  decode(ct6, e68)

}